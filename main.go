package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/lunny/config"
	"github.com/lunny/tango"
)

type Action struct {
	tango.Ctx
	tango.Params
}

var (
	cfgpath = flag.String("cfg", "./cfg.ini", "config file path")
)

var (
	hosts = map[string]bool{}
)

func loadHosts(cfg *config.Config) {
	hts := cfg.Get("hosts")
	hs := strings.Split(hts, ";")
	for _, h := range hs {
		hosts[h] = true
	}
}

func (a *Action) Do() error {
	a.Debug("request host: ", a.Req().Host)
	if _, ok := hosts[a.Req().Host]; ok {
		var sechme = "http"
		if a.Req().Host == "go.googlesource.com" {
			sechme = "https"
		}
		rl := sechme + "://" + a.Req().Host + a.Req().URL.String()
		a.Debug("request:", rl)
		req, err := http.NewRequest(a.Req().Method, rl, a.Req().Body)
		if err != nil {
			return err
		}

		for k, v := range a.Ctx.Req().Header {
			for _, vv := range v {
				req.Header.Add(k, vv)
			}
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		for k, v := range resp.Header {
			for _, vv := range v {
				a.Ctx.Header().Add(k, vv)
			}
		}
		_, err = io.Copy(a.Ctx, resp.Body)
		if err != nil {
			return err
		}
	} else {
		_, err := a.Write([]byte("Only support proxy github.com & code.google.com"))
		return err
	}
	return nil
}

func (a *Action) Get() error {
	return a.Do()
}

func (a *Action) Post() error {
	return a.Do()
}

func main() {
	flag.Parse()

	cfg, err := config.Load(*cfgpath)
	if err != nil {
		fmt.Println(err)
		return
	}

	loadHosts(cfg)

	listens := strings.Split(cfg.Get("listen"), ";")

	if len(listens) == 2 {
		t1 := tango.Classic()
		t1.Any("/*name", new(Action))
		go t1.RunTLS("./cert.pem", "./key.pem", listens[1])
	}

	t := tango.Classic()
	t.Any("/*name", new(Action))
	t.Run(listens[0])
}
