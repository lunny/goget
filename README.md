# How to use

There are three steps:

1. `go get github.com/lunny/goget` and build the binary
2. upload goget, cfg.ini to your server and run it.
3. change your hosts golang.org & go.googlesource.com to your IP.

And then

```
go get -insecure golang.org/x/mobile/cmd/gomobile
```

It's work.